import express, { Application } from "express"
import postRouter from "./routes/posts"
import authRouter from "./routes/auth"
import { MONGO_OPTIONS, MONGO_URI } from "./config/db"
import bodyParser from "body-parser"
import { checkAuthentication } from "./middleware/auth"
import { config } from "dotenv"
import cookieParser from "cookie-parser"
import allowCrossDomain from "./middleware/cors"
import { connect } from "mongoose"

const app: Application = express()

config()

const PORT = process.env.APP_PORT || 5000

// Middleware section
app.use(allowCrossDomain)
app.use(bodyParser.json())
app.use(cookieParser())

app.use('/posts', checkAuthentication, postRouter)
app.use('/auth', authRouter)

const start = async () => {
        await connect(MONGO_URI, MONGO_OPTIONS)
        // tslint:disable-next-line:no-console
        app.listen(PORT, () => console.log(`Server running on http://localhost:${PORT}`))
}
// tslint:disable-next-line:no-console
start().catch(err => console.error(err.message))

