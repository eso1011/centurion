import { NextFunction, Request, Response } from "express"

const whiteList = [
    "http://localhost:5000",
]
const allowCrossDomain = (req: Request, res: Response, next: NextFunction) => {
    if (whiteList.indexOf(req.headers.origin as string) !== -1) {
        res.header('Access-Control-Allow-Credentials', "true")
        res.header('Access-Control-Allow-Origin', req.headers.origin)
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS')
        res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With, Origin, Accept')
        next()
    } else {
        res.status(500).send({ "message": "Not allowed by CORS" })
    }
}

export default allowCrossDomain
