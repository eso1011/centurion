import { NextFunction, Request, Response } from "express"
import jwt from "jsonwebtoken"

export const checkAuthentication = (request: Request, response: Response, next: NextFunction) => {
    const token = getHeaderBearerToken(request)
    if (token == null) return response.sendStatus(401)

    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET as string, (err) => {
        if (err) return response.status(403).json({ message: err.message })
        next()
    })
}

const getHeaderBearerToken = (request: Request): string | undefined => {
    // Authorization: Bearer <token>
    const authHeader: string | undefined  = request.headers.authorization
    return authHeader && authHeader.split(' ')[1]
}



