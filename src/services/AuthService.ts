import jwt from "jsonwebtoken"
import bcrypt from "bcryptjs"
import { User } from "../models/User"
import { v4 as uuidv4 } from "uuid"

interface IUser {
    email: string,
    password: string,
    jti: string
}

interface IAuthTokens {
    accessToken: string,
    refreshToken: string,
}

export class AuthService {
    ACCESS_SECRET = process.env.ACCESS_TOKEN_SECRET as string
    REFRESH_SECRET = process.env.REFRESH_TOKEN_SECRET as string

    authenticate(email: string, password: string) : IAuthTokens {
        const user: IUser = { jti: "", password, email }
        const accessToken = this.generateToken(user)
        const refreshToken = this.generateToken(user, this.REFRESH_SECRET)
        return { accessToken, refreshToken }

    }

    async register(email: string, password: string) : Promise<IUser> {
        const hashedPassword = await bcrypt.hash(password, 12)
        const candidate = await User.findOne({ email })
        const jti = uuidv4()

        return new Promise((resolve, reject) => {
            if (candidate) {
                reject("Already registered")
            }
            const user: IUser = { email, password: hashedPassword, jti }
            const model = new User(user)
            model.save()
            resolve(user)
        })
    }

     generateToken(user: IUser, secret: string = this.ACCESS_SECRET): string {
        return jwt.sign(user, secret, {
            expiresIn: '15s'
        })
    }
}
