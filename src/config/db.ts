const {
    MONGO_USERNAME,
    MONGO_PASSWORD,
    MONGO_HOSTNAME,
    MONGO_PORT,
    MONGO_DB
} = process.env

export const MONGO_OPTIONS = {
    useNewUrlParser: true,
    reconnectTries: 3,
    reconnectInterval: 500,
    connectTimeoutMS: 10000,
    useCreateIndex: true,
    useUnifiedTopology: true,
}

export const MONGO_URI = `mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOSTNAME}:${MONGO_PORT}/${MONGO_DB}?authSource=admin`
