import express, { Request, Response, Router } from "express"
import jwt from "jsonwebtoken"
import { AuthService } from "../services/AuthService"

const authRouter: Router = express.Router()
const ACCESS_SECRET = process.env.ACCESS_TOKEN_SECRET as string
const REFRESH_SECRET = process.env.REFRESH_TOKEN_SECRET as string

interface IUser {
    email: string,
    password: string,
    jti: string
}

interface IJwtPayload extends IUser {
    iat: string,
    exp?: number,
}

authRouter.post('/sign_in', (req: Request, res: Response) => {
    const { email, password } = req.body
    const service = new AuthService()
    const tokens = service.authenticate(email, password)
    res.setHeader('Set-Cookie', `refresh_token=${tokens.refreshToken}; HttpOnly`)
    return res.json(tokens)
})

authRouter.post('/sign_up', async (req: Request, res: Response) => {
    const { email, password } = req.body
    const service = new AuthService()

    service.register(email, password).then(() => {
        const tokens = service.authenticate(email, password)
        res.setHeader('Set-Cookie', `refresh_token=${tokens.refreshToken}; HttpOnly`)
        return res.status(201).json(tokens)
    }).catch((message) => {
        res.status(422).json({ message })
    })

})

authRouter.post("/logout", (req: Request, res: Response) => {
    res.json({ message: "Logout" })
})

authRouter.post("/refresh", (req: Request, res: Response) => {
    const refreshToken: string = req.cookies.refresh_token
    jwt.verify(refreshToken, REFRESH_SECRET, (err, payload) => {
        if (err) return res.status(403).json({ message: err.message })
        const jwtPayload = payload as IJwtPayload
        const user: IUser = { jti: "", password: "", email: jwtPayload.email }
        const accessToken: string = generateToken(user)
        return res.json({ accessToken })
    })

})

const generateToken = (user: IUser, secret: string = ACCESS_SECRET): string => {
    return jwt.sign(user, secret, {
        expiresIn: '15s'
    })
}

export default authRouter
