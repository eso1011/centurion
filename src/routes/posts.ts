import express, { Request, Response, Router } from "express"

const postRouter: Router = express.Router()

postRouter.get("/", (req: Request, res: Response) => {
    res.send("All posts")
})

postRouter.get("/:postId", (req: Request, res: Response) => {
    res.json({ "message": `Specific posts ${req.params.postId}` })
})

export default postRouter
