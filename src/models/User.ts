import { model, Schema } from "mongoose"


const schema = new Schema({
    email: { type: String, required: true, unique: true },
    jti: { type: String, required: true, unique: true },
    password: { type: String, required: true },
})

export const User = model("User", schema)
